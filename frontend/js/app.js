// variabler!
var refreshInterval = 60000; // ms - hvor ofte skal vi checke efter nye opdateringer!
var slideTimeout = 30000;
var debug = true; // if true - viser console.log prints
var twitterTemplate = '<div class="twitter"><div class="header"><img src="%USRIMG%" style="display: inline-block; vertical-align: middle;"> <p style="display: inline-block;">%USRNAME% <span style="color: lightgrey;">%USRAT%</span></p></div><div class="image">%TWEETIMG%</div><div class="tweet" style="padding: 20px;"><p style="font-size: 2em;">%TWEET%</p></div><div class="meta"><img src="./img/retweet.png" style="height: 1em; width: auto; display: inline-block; vertical-align: middle;"> <span id="retweets">%RETWEETS%</span> <img src="./img/heart.png" style="height: 1em; width: auto; display: inline-block; vertical-align: middle;"> <span id="tweethearts">%TWEETHEARTS%</span></div></div>';
//var instagramTemplate = '<div class="instagram"><div class="header"><img src="%USRIMG%" style="display: inline-block"> <p style="display: inline-block;">%USRNAME%</p><div style="float: right"><span style="color: red;">❤</span> %LIKES% 💬 %COMMENTS%</div></div><div class="image" style="float: left; width: 70%;">%MEDIA%</div><div class="meta" style="float: left; width: 30%;"><div class="meta_content" style="padding: 5%; height: %METAHEIGHT%px;">%META%</div></div></div>';

// %USRIMG%, %USRNAME%, %LIKES%, %COMENTS%, %MEDIA%, %META%
var instagramTemplate = '<div class="instagram"><div class="header"><img src="%USRIMG%" style="display: inline-block"> <p style="display: inline-block;">%USRNAME%</p><div style="float: right"><img src="./img/heart.png" style="height: 1em; width: auto; display: inline-block; vertical-align: middle;"> %LIKES% <img src="./img/comment.png" style="display: inline-block; height: 1em; width: auto; vertical-align: middle;"> %COMMENTS%</div></div><div class="image">%MEDIA%</div><div class="meta"><div class="meta_content">%META%</div></div></div>';

$(document).ready(function(){
  if(debug)
    console.log("ready...");

  grabInstagram();
  grabTwitter();
  runTime();
 // moment.locale('da');

  // connect til websocket serveren...

               var ws = new WebSocket("ws://zserver.cf:10000");

               ws.onopen = function()
               {
                  // Web Socket is connected, send data using send()
                  ws.send("hello!");
                  //alert("Message is sent...");
               };

               ws.onmessage = function (evt)
               {
                  var received_msg = evt.data;
                  var data = JSON.parse(evt.data);
                  console.log("Data", data);
                  if(data.type == "announcement"){
                    console.log("showing!");
                    var ann = {
                      title: data.data.title,
                      text: data.data.text,
                      time: data.data.time
                    };
                    console.log("Ann", ann);
                    showAnnouncement(ann, parseInt(data.data.duration)*1000);
                  }

                  if(data.type == "reload"){
                    window.location.reload();
                  }
                  //alert("Message is received...");
               };

               ws.onclose = function()
               {
                  // websocket is closed.
                  //alert("Connection is closed...");
               };

});

var igContent = [];
var tweets = [];
var instaIndex = 0;
var twitterIndex = 0;

var cb = new Codebird();
cb.setConsumerKey('UKJGGvWmCYdS8kNZ2qlLzzZxG', 'qYHvS2nohjD7HckXrJH52NHuDNvN1tv8RERhnqUENDcMP8OJYl');

function grabTwitter(){
  var tmpFeed = new Array();
  cb.__call(
    "search_tweets", {q: "auhack", tweet_mode: "extended"},
    function(reply){
      if(debug)
        console.log(reply);
      if(reply.httpstatus == 200){
          for(var x in reply.statuses){
            tmpFeed.push(reply.statuses[x]);

          }
          cb.__call(
            "search_tweets", {q: "auhack2017", tweet_mode: "extended"},
            function(reply){
              if(debug)
                console.log(reply);
              if(reply.httpstatus == 200){

                  for(var x in reply.statuses){
                    tmpFeed.push(reply.statuses[x]);
                  }
                  tweets = tmpFeed;
              }
            }
          );
      }
    }
  );

}

function grabInstagram() {
  var tmpFeed = new Array();
  var feed1 = new Instafeed({
    get: 'tagged',
    tagName: 'auhack',
    userId: '1706769936',
    accessToken: '1706769936.ba4c844.85ecb2e61c8449bf86c42996ad2c8b5c',
    success: function(data){
      for(var x in data.data){
        tmpFeed.push(data.data[x]);
      }
      var feed2 = new Instafeed({
        get: 'tagged',
        tagName: 'auhack2017',
        userId: '1706769936',
        accessToken: '1706769936.ba4c844.85ecb2e61c8449bf86c42996ad2c8b5c',
        success: function(data){
          for(var x in data.data){
            tmpFeed.push(data.data[x]);
          }
          igContent = tmpFeed;
          if(debug)
            console.log(igContent);
        },
        error: function(data){
          if(debug)
            console.log("Feed2 Error", data);

          igContent = tmpFeed;
          if(debug)
            console.log(igContent);
        }
      });
      feed2.run();
    },
    error: function(data){
      if(debug)
        console.log("Feed 1 error", data);
    }
  });
  feed1.run();

}

function showAnnouncement(announcement, timeout = 10000){

  $('.dimmer').fadeIn('fast');
  var announcementHtml = "<h1>"+announcement.title+"</h1>"
                      +"<hr>"
                      +"<p style='font-size: 1.2em;'>"+announcement.time+"</p><br><br>"
                      +"<p>"+announcement.text+"</p>";
  $('.announcement').html(announcementHtml);
  $('.announcement').fadeIn('fast');
  setTimeout(function(){
      $('.dimmer').fadeOut('fast');
      $('.announcement').fadeOut('fast', function(){
        $('.announcement').html("");
      });
  }, timeout);
}

function nextSlide(){
  // what to show next??
  if(igContent.length == 0){
    var nxt_topic = 2;
  } else if(tweets.length == 0){
    var nxt_topic = 1;
  } else {
    var nxt_topic = Math.floor(Math.random() * 2) + 1;
  }


  if(nxt_topic == 1){
    // instagram
    var nextIndex = instaIndex+1;
    if(nextIndex > igContent.length)
      nextIndex = 0;

    instaIndex = nextIndex;
    return igContent[instaIndex];
  } else {
    // twitter
    var nextIndex = twitterIndex+1;
    if(nextIndex > tweets.length)
      nextIndex = 0;

    twitterIndex = nextIndex;
    return tweets[twitterIndex]
  }

}
// set interval til at opdatere feeds
var updateFeeds = setInterval(function(){
  grabTwitter();
  grabInstagram();
}, refreshInterval);

function refreshPage(){
  window.location.reload();
}

// kør selve slideshowet

function runTime(){
  setInterval(() => {
    $('#clock').html(moment().format("dddd, Do MMMM YYYY, HH:mm:ss").capitalizeFirstLetter());
  }, 1000);
}

function displaySlide(){
  var slide = nextSlide();
  var isVideo = false;
  // check at der faktisk er et slide - ellers prøv igen!
  // hvis slides er ved at opdatere kan vi få null værdier!
  if(slide == null){
    displaySlide();
    return;
  }

  // er det twitter eller instagram?
  if("source" in slide){
    // twitter
    if(debug)
      console.log("Twitter!");
    // if img margin-top 3, else 13 :) i vh
    // hiv de info ud vi skal bruge og smid dem ind i templaten
    // %USRIMG%, %USRNAME%,%USRAT%,%TWEETIMG%, %TWEET%, %RETWEETS%, %TWEETHEARTS%
    var tmp = twitterTemplate;
    tmp = tmp.replace('%USRIMG%', slide.user.profile_image_url);
    tmp = tmp.replace('%USRNAME%', slide.user.name);
    tmp = tmp.replace('%USRAT%', slide.user.screen_name);
    tmp = tmp.replace('%TWEET%', slide.full_text);
    tmp = tmp.replace('%RETWEETS%', slide.retweet_count);
    tmp = tmp.replace('%TWEETHEARTS%', slide.favorite_count);

    // if check for img!
    var hasImage = false;
    console.log("Slide", slide);
    if("media" in slide.entities){
      console.log("Media!", slide.entities.media[0].media_url);
      tmp = tmp.replace('%TWEETIMG%', '<img src="'+slide.entities.media[0].media_url+'" alt="twitimg">');
      hasImage = true;
    }
    if(tmp.indexOf('%TWEETIMG%') !== -1){
      tmp = tmp.replace('%TWEETIMG%', "");
    }


    console.log(tmp);
  } else {
    // instagram
    if(debug)
      console.log("Instagram!");

      var tmp = instagramTemplate;
      // %USRIMG%, %USRNAME%, %LIKES%, %COMENTS%, %MEDIA%, %META%
      tmp = tmp.replace('%USRIMG%', slide.user.profile_picture);
      tmp = tmp.replace('%USRNAME%', slide.user.username);
      tmp = tmp.replace('%LIKES%', slide.likes.count);
      tmp = tmp.replace('%COMMENTS%', slide.comments.count);
      tmp = tmp.replace('%META%', slide.caption.text);

      if(slide.type == "image"){
        //var media = "<img src='"+slide.images.standard_resolution.url+"'>"; //style='height: "+slide.images.standard_resolution.height+"px; width: "+slide.images.standard_resolution.width+"px;'>";
        var media = "<div style='width: 640px; height: 640px; background-image: url(\""+slide.images.standard_resolution.url+"\"); background-size: cover;'></div>";
        //tmp = tmp.replace('%METAHEIGHT%', slide.images.standard_resolution.height+30);
      } else if(slide.type == "video"){
        var media = "<video id='videoplayer' autoplay='autoplay' looping src='"+slide.videos.standard_resolution.url+"'>";
        //tmp = tmp.replace('%METAHEIGHT%', slide.videos.standard_resolution.height+30);

        // is video!
        isVideo = true;
      }
      tmp = tmp.replace('%MEDIA%', media);
  }

  $('.feed').fadeOut('fast', function(){
    $('.feed').html(tmp);
    // check om video go sæt timeout til videolængden
    if(isVideo){
      var video = $('#videoplayer').get(0);
      document.getElementById('videoplayer').addEventListener('loadeddata', function() {
        var newTimeout = video.duration;
        clearInterval(displayLoop);
      });
      document.getElementById('videoplayer').addEventListener('ended', function() {
        if(debug)
          console.log("Video Ended!");
          displaySlide();
          displayLoop = setInterval(displaySlide, slideTimeout);

      });
    }
    $('.feed').fadeIn('fast');
  });

}


var displayLoop = setInterval(displaySlide, slideTimeout);

String.prototype.capitalizeFirstLetter = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
}
