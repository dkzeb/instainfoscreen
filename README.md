# InstaInfoScreen #

### What is this repository for? ###

The InstaGram/Twitter InfoScreen with announcements project by ME!

### How do I get set up? ###

* Get the code, deploy on the servers
* Put in your credentials generated from the API's via Twitter/Instagram
* Put in the tags people are supposed to post to
* Change the background to match something you like

Have fun!